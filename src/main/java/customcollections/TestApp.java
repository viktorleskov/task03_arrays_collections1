package customcollections;

import customcollections.arraylistcustom.ArrayListCustom;
import customcollections.comparabletask.Twostring;
import customcollections.utils.StringGenerator;

import java.util.ArrayList;
import java.util.List;

public class TestApp {
    public static void main(String[] args) {
        //test1();
        test2();
    }
    private static void test1() {  //compare performance
        int TEST_SIZE = 20;
        long startTime;
        long endTime;

        ArrayListCustom testList1 = new ArrayListCustom();
        List testList2 = new ArrayList<String>();
        //Custom ArrayList test
        startTime = System.nanoTime();
        for (int k = 0; k < TEST_SIZE; k++) {
            testList1.add("Hi" + k);
        }
        endTime = System.nanoTime();
        System.out.println("Custom ArrayList time(nano):" + (endTime - startTime));
        //Standart ArrayList test
        startTime = System.nanoTime();
        for (int k = 0; k < TEST_SIZE; k++) {
            testList2.add("Hi" + k);
        }
        endTime = System.nanoTime();
        System.out.println("Default ArrayList time(nano):" + (endTime - startTime));
    }
    public static void test2(){ //compare test
        int DEFAULT_LENTH = 5;
        Twostring first = new Twostring(StringGenerator.generateRandomString(DEFAULT_LENTH),
                StringGenerator.generateRandomString(DEFAULT_LENTH));
        Twostring second = new Twostring(first.getFirstString(),
                StringGenerator.generateRandomString(DEFAULT_LENTH));

        System.out.println(first.getFirstString()+" "+first.getSecondString());
        System.out.println(second.getFirstString()+" "+second.getSecondString());
        System.out.println(first.equals(second));
    }
}
