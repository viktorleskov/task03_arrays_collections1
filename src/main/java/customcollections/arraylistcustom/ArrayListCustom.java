package customcollections.arraylistcustom;

public class ArrayListCustom {
    private static final int DEFAULT_SIZE=5;
    private static final int RESIZE_STEP=2;
    private String[] strArray;
    private int arraySize;

    public ArrayListCustom(){
        strArray = new String[DEFAULT_SIZE];
        arraySize=DEFAULT_SIZE;
    }
    private void resizeArray(int newSize){
        arraySize=newSize;
        String[] newArray = new String[newSize];
        for(int k=0; k<strArray.length; k++){
            newArray[k]=strArray[k];
        }
        strArray=newArray;
    }
    private int calculateNoNullElements(){
        int nonull=0;
        for(int k=0; k<arraySize;k++){
            if(strArray[k]!=null){
                nonull++;
            }
        }
        return nonull;
    }
    public void add(String addedString){
        if(strArray[strArray.length-1]!=null){
            resizeArray(arraySize+RESIZE_STEP);
        }
        for(int k=0; k<strArray.length; k++){
            if(strArray[k]==null){
                strArray[k]= addedString;
                return;
            }
        }
    }
    public String get(int index){
        return strArray[index];
    }
    public int size(){
        return calculateNoNullElements();
    }
    public boolean equals(Object o){
        return this.get(0).equals(((ArrayListCustom)o).get(0));
    }
}
