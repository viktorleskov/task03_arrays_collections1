package customcollections.comparabletask;

import customcollections.arraylistcustom.ArrayListCustom;

public class Twostring {
    private ArrayListCustom arrlist;

    public Twostring(String first, String second){
        arrlist = new ArrayListCustom();
        arrlist.add(first);
        arrlist.add(second);
    }
    public String getFirstString(){
        return arrlist.get(0);
    }
    public String getSecondString(){
        return arrlist.get(1);
    }
    public boolean equals(Twostring another){
        return arrlist.get(0).equals(another.getFirstString());
    }
}
