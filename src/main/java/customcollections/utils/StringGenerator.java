package customcollections.utils;

import java.util.Random;

public class StringGenerator {
    private StringGenerator(){}
    public static String generateRandomString(int length){
        Random rand = new Random();
        String buf = new String("");
        for(int k=0;k<length;k++){
            buf+=(char)(65+rand.nextInt(5));
        }
        return buf;
    }
}
